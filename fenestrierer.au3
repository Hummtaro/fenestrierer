; kekse23.de Fenestrierer
; Copyright (c) 2017, Nicholas Regitz
;
; Diese Datei ist Lizensiert unter der Creative Commons 4.0 CC BY-NC-SA
; https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode

#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=fenestrierer.ico
#AutoIt3Wrapper_Outfile=Fenestrierer.exe
#AutoIt3Wrapper_Outfile_x64=Fenestrierer_x64.exe
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Res_Comment=Versetzt Fenster in einen rahmenlosen Fenstermodus
#AutoIt3Wrapper_Res_Description=Fenestrierer by Hummtaro
#AutoIt3Wrapper_Res_Fileversion=1.0.2.1
#AutoIt3Wrapper_Res_ProductVersion=1.0
#AutoIt3Wrapper_Res_LegalCopyright=Copyright © 2017 Nicholas Regitz
#AutoIt3Wrapper_Res_Language=1031
#AutoIt3Wrapper_Run_Tidy=y
#AutoIt3Wrapper_Run_Au3Stripper=y
#Au3Stripper_Parameters=/PE /RM /RSLN
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#include <Constants.au3>
#include <WindowsConstants.au3>
#include <WinAPI.au3>
#include <WinAPISys.au3>
#include <WinAPIRes.au3>
#include <Array.au3>
#include <Misc.au3>

#include <ButtonConstants.au3>
#include <ComboConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <GUIListBox.au3>
#include <GuiStatusBar.au3>
#include <StaticConstants.au3>

Global $IniFile = @ScriptDir & "\fenestrierer.ini"
Global $LoadIni = 1

Global $ProgName[1]
Global $ProgStat[1]
Global $ProgPosX[1]
Global $ProgPosY[1]
Global $ProgResX[1]
Global $ProgResY[1]
Global $ProgBfix[1]
Global $ProgWait[1]
Global $ProgMode[1]
Global $ProgMatch[1]
Global $ProgClass[1]

Global $MatchModes[4] = ["Anfang", "Ende", "Mitte", "Genau"]

KillOld()

Opt("TrayMenuMode", 3)
Opt("TrayOnEventMode", 1)
Global $iAutostart = TrayCreateItem("Autostart", -1, -1, 1)
TrayItemSetOnEvent(-1, "TrayAutorun")
TrayCreateItem("", -1, -1, -1)
Global $iReload = TrayCreateItem("Neu laden", -1, -1, -1)
TrayItemSetOnEvent(-1, "TrayReload")
Global $iConfig = TrayCreateItem("Konfigurieren", -1, -1, -1)
TrayItemSetOnEvent(-1, "TrayConfig")
Global $iAbout = TrayCreateItem("Über Fenestrierer", -1, -1, -1)
TrayItemSetOnEvent(-1, "TrayAbout")
Global $iExit = TrayCreateItem("Beenden", -1, -1, -1)
TrayItemSetOnEvent(-1, "TrayExit")

Global $Autostart = 0

RegRead("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Run", "Fenestrierer by Hummtaro")
If Not @error Then
	$Autostart = 1
	TrayItemSetState($iAutostart, $TRAY_CHECKED)
EndIf

Opt("GUIOnEventMode", 1)
$Form1 = GUICreate("Fenestrierer", 522, 354, 229, 250, -1, BitOR($WS_EX_TOOLWINDOW, $WS_EX_WINDOWEDGE, $WS_EX_APPWINDOW, $WS_EX_TOPMOST))
GUISetOnEvent($GUI_EVENT_CLOSE, "Form1Close")
$ListEntries = GUICtrlCreateList("", 8, 8, 177, 279)
GUICtrlSetOnEvent(-1, "ListClick")
$ButtonAdd = GUICtrlCreateButton("Hinzufügen", 8, 296, 89, 25)
GUICtrlSetOnEvent(-1, "ButtonAddClick")
$ButtonDel = GUICtrlCreateButton("Entfernen", 96, 296, 89, 25)
GUICtrlSetOnEvent(-1, "ButtonDelClick")
$Label1 = GUICtrlCreateLabel("Fenster Titel", 200, 16, 62, 17)
$InputTitle = GUICtrlCreateInput("", 192, 32, 321, 21)
$Label9 = GUICtrlCreateLabel("Übereinstimmung:", 368, 11, 82, 17, $SS_CENTER)
$ComboMatch = GUICtrlCreateCombo("", 451, 8, 62, 25, BitOR($CBS_DROPDOWNLIST, $CBS_AUTOHSCROLL))
GUICtrlSetData(-1, "Anfang|Ende|Mitte|Genau", "Genau")
$Label2 = GUICtrlCreateLabel("Fenster Klasse (Leer lassen um nur Titel zu prüfen)", 200, 72, 241, 17)
$InputClass = GUICtrlCreateInput("", 192, 88, 321, 21)
$Label3 = GUICtrlCreateLabel("Auflösung", 208, 128, 51, 17, $SS_CENTER)
$Label5 = GUICtrlCreateLabel("Null für auto", 208, 168, 61, 17)
$InputResX = GUICtrlCreateInput("0", 192, 144, 41, 21, BitOR($GUI_SS_DEFAULT_INPUT, $ES_CENTER, $ES_NUMBER))
GUICtrlSetLimit(-1, 5)
$InputResY = GUICtrlCreateInput("0", 240, 144, 41, 21, BitOR($GUI_SS_DEFAULT_INPUT, $ES_CENTER, $ES_NUMBER))
GUICtrlSetLimit(-1, 5)
$Label4 = GUICtrlCreateLabel("Position", 326, 128, 41, 17)
$Label6 = GUICtrlCreateLabel("Von oben links", 310, 168, 74, 17)
$InputPosX = GUICtrlCreateInput("0", 302, 144, 41, 21, BitOR($GUI_SS_DEFAULT_INPUT, $ES_CENTER))
GUICtrlSetLimit(-1, 5)
$InputPosY = GUICtrlCreateInput("0", 350, 144, 41, 21, BitOR($GUI_SS_DEFAULT_INPUT, $ES_CENTER))
GUICtrlSetLimit(-1, 5)
$Label8 = GUICtrlCreateLabel("Delay (ms)", 448, 131, 78, 17)
$InputDelay = GUICtrlCreateInput("0", 412, 128, 34, 21, BitOR($GUI_SS_DEFAULT_INPUT, $ES_CENTER, $ES_NUMBER))
GUICtrlSetLimit(-1, 5)
$Label10 = GUICtrlCreateLabel("Rand fix (px)", 448, 163, 63, 17)
$InputBorderFix = GUICtrlCreateInput("0", 412, 160, 34, 21, BitOR($GUI_SS_DEFAULT_INPUT, $ES_CENTER, $ES_NUMBER))
GUICtrlSetLimit(-1, 4)
$Label7 = GUICtrlCreateLabel("Kompalibitätsmodi", 312, 208, 88, 17)
$CheckResize = GUICtrlCreateCheckbox("Force resize", 192, 232, 88, 17)
$CheckActive = GUICtrlCreateCheckbox("Set active", 320, 232, 88, 17)
$CheckMouseHide = GUICtrlCreateCheckbox("Hide mouse", 440, 232, 88, 17)
$CheckMove = GUICtrlCreateCheckbox("Force move", 192, 256, 88, 17)
$CheckFocus = GUICtrlCreateCheckbox("Set focus", 320, 256, 88, 17)
$CheckMouseTrap = GUICtrlCreateCheckbox("Trap mouse", 440, 256, 88, 17)
$CheckExstyle = GUICtrlCreateCheckbox("Keep exstyle", 192, 280, 88, 17)
$CheckNotitle = GUICtrlCreateCheckbox("No title", 320, 280, 88, 17)
$Save = GUICtrlCreateButton("Speichern", 432, 296, 81, 25)
GUICtrlSetOnEvent(-1, "SaveClick")
$StatusBar1 = _GUICtrlStatusBar_Create($Form1)
Dim $StatusBar1_PartsWidth[3] = [256, 384, -1]
_GUICtrlStatusBar_SetParts($StatusBar1, $StatusBar1_PartsWidth)
_GUICtrlStatusBar_SetText($StatusBar1, " Fenestrierer by Hummtaro © 2017", 0)
_GUICtrlStatusBar_SetText($StatusBar1, @TAB & FileGetVersion(@ScriptFullPath), 1)
_GUICtrlStatusBar_SetText($StatusBar1, @TAB & "https://kekse23.de.", 2)
_GUICtrlStatusBar_SetMinHeight($StatusBar1, 17)
GUISetState(@SW_HIDE)

LoadIni()

Global $MouseHidden = False
Global $MouseTrapped = False
Global $BlankCursor

While 1
	If $LoadIni = 1 Then
		LoadIni()
		$LoadIni = 0
	EndIf

	$WinArrID = -1
	$Window = WinGetHandle("")
	For $i = 0 To UBound($ProgName) - 1 Step +1
		If CompareTitle(WinGetTitle($Window), $ProgName[$i], $ProgMatch[$i]) Then
			If $ProgClass[$i] <> "" Then
				If CompareTitle(_WinAPI_GetClassName($Window), $ProgClass[$i], $ProgMatch[$i]) Then
					$WinArrID = $i
				EndIf
			Else
				$Class = _WinAPI_GetClassName($Window)
				If $Class <> "CabinetWClass" And $Class <> "MozillaWindowClass" And $Class <> "IEFrame" And $Class <> "Chrome_WidgetWin_1" And $Class <> "FM" Then
					$WinArrID = $i
				EndIf
			EndIf
		ElseIf BitAND($ProgMode[$i], 128) > 0 And $ProgClass[$i] <> "" Then
			If CompareTitle(_WinAPI_GetClassName($Window), $ProgClass[$i], $ProgMatch[$i]) Then
				$WinArrID = $i
			EndIf
		EndIf
		If Not WinExists($ProgStat[$i]) Then
			$ProgStat[$i] = 0
		EndIf
		Sleep(1)
	Next

	If $WinArrID <> -1 Then
		If $ProgStat[$WinArrID] <> 0 Then
			If BitAND($ProgMode[$WinArrID], 4) > 0 And Not $MouseHidden Then
				$MouseHidden = True
				If Not $BlankCursor Then
					FileInstall("blankcursor.cur", @TempDir & "\_blank.cur", 1)
					$BlankCursor = _WinAPI_LoadCursorFromFile(@TempDir & "\_blank.cur")
				EndIf
				_WinAPI_SetSystemCursor($BlankCursor, $OCR_NORMAL, True)
			EndIf
			If BitAND($ProgMode[$WinArrID], 32) And Not $MouseTrapped Then
				$MouseTrapped = True
				_MouseTrap($ProgPosX[$WinArrID], $ProgPosY[$WinArrID], $ProgPosX[$WinArrID] + $ProgResX[$WinArrID], $ProgPosY[$WinArrID] + $ProgResY[$WinArrID])
			EndIf
			If BitAND(_WinAPI_GetWindowLong($Window, $GWL_STYLE), $WS_POPUP) <> $WS_POPUP Then
				$ProgStat[$WinArrID] = 0
			EndIf
		EndIf

		If $ProgStat[$WinArrID] = 0 Then
			Sleep($ProgWait[$WinArrID] + 1)
			If BitAND($ProgMode[$WinArrID], 64) < 1 Then _WinAPI_SetWindowLong($Window, $GWL_EXSTYLE, 0x0)
			_WinAPI_SetWindowLong($Window, $GWL_STYLE, $WS_POPUP)
			If BitAND($ProgMode[$WinArrID], 1) > 0 Then _WinAPI_SetWindowPos($Window, $HWND_TOP, $ProgPosX[$WinArrID], $ProgPosY[$WinArrID], $ProgResX[$WinArrID] / 2, $ProgResY[$WinArrID] / 2, $SWP_SHOWWINDOW)
			If BitAND($ProgMode[$WinArrID], 8) > 0 Then _WinAPI_SetWindowPos($Window, $HWND_TOP, $ProgPosX[$WinArrID] + 128, $ProgPosY[$WinArrID] + 128, $ProgResX[$WinArrID], $ProgResY[$WinArrID], $SWP_SHOWWINDOW)
			$b = $ProgBfix[$WinArrID]
			_WinAPI_SetWindowPos($Window, $HWND_TOP, $ProgPosX[$WinArrID] - $b, $ProgPosY[$WinArrID] - $b, $ProgResX[$WinArrID] + $b + $b, $ProgResY[$WinArrID] + $b + $b, $SWP_SHOWWINDOW)
			If BitAND($ProgMode[$WinArrID], 2) > 0 Then _WinAPI_SetActiveWindow($Window)
			If BitAND($ProgMode[$WinArrID], 16) > 0 Then _WinAPI_SetFocus($Window)
			$ProgStat[$WinArrID] = $Window
		EndIf
	Else
		If $MouseHidden Then
			$MouseHidden = False
			_WinAPI_SystemParametersInfo(0x0057)
		EndIf
		If $MouseTrapped Then
			$MouseTrapped = False
			_MouseTrap()
		EndIf
	EndIf

	Sleep(100)
WEnd

Func TrayAutorun()
	If $Autostart <> 1 Then
		RegWrite("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Run", "Fenestrierer by Hummtaro", "REG_SZ", '"' & @ScriptFullPath & '"')
		TrayItemSetState($iAutostart, $TRAY_CHECKED)
		$Autostart = 1
	Else
		RegDelete("HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Run", "Fenestrierer by Hummtaro")
		TrayItemSetState($iAutostart, $TRAY_UNCHECKED)
		$Autostart = 0
	EndIf
EndFunc   ;==>TrayAutorun

Func TrayReload()
	$LoadIni = 1
EndFunc   ;==>TrayReload

Func TrayConfig()
	GUISetState(@SW_HIDE)
	RefreshList()
	GUISetState(@SW_SHOW)
EndFunc   ;==>TrayConfig

Func TrayAbout()
	MsgBox($MB_SYSTEMMODAL, "Fenestrierer - About", "Fenestrierer by Hummtaro" & @CRLF & _
			"Version: " & FileGetVersion(@ScriptFullPath) & @CRLF & @CRLF & _
			"Copyright © 2017 Nicholas Regitz")
EndFunc   ;==>TrayAbout

Func TrayExit()
	Exit
EndFunc   ;==>TrayExit

Func ListClick()
	RefreshGui()
EndFunc   ;==>ListClick

Func ButtonAddClick()
	ToolTip("Bitte zu Fenestrierendes Fenster anklicken", MouseGetPos(0), MouseGetPos(1), "Fenestrierer", 1, 0)
	$Count = 0
	Local $hDLL = DllOpen("user32.dll")
	While 1
		If _IsPressed("01", $hDLL) Then
			ToolTip("")
			Sleep(100)
			$NewWin = WinGetHandle("")
			$Titel = WinGetTitle($NewWin)
			If $Titel <> "" Then
				$Match = 3
				While 1
					If StringRight($Titel, 1) = " " Then
						$Titel = StringTrimRight($Titel, 1)
						$Match = 0
					ElseIf StringLeft($Titel, 1) = " " Then
						$Titel = StringTrimLeft($Titel, 1)
						If $Match = 0 Then
							$Match = 2
						Else
							$Match = 1
						EndIf
					Else
						ExitLoop
					EndIf
				WEnd
				IniWrite($IniFile, $Titel, "Class", _WinAPI_GetClassName($NewWin))
				If $Match <> 3 Then IniWrite($IniFile, $Titel, "Match", $Match)
			ElseIf _WinAPI_GetClassName($NewWin) <> "" Then
				$Titel = "Fenster ohne Titel " & Random(1000, 9999, 1)
				IniWrite($IniFile, $Titel, "Class", _WinAPI_GetClassName($NewWin))
				IniWrite($IniFile, $Titel, "Mode", "128")
			EndIf
			GUISetState(@SW_HIDE)
			RefreshList()
			GUICtrlSetData($ListEntries, $Titel)
			RefreshGui()
			GUISetState(@SW_SHOW)
			ExitLoop
		EndIf
		Sleep(10)
		$Count += 1
		If $Count > 1024 Then
			ToolTip("")
			ExitLoop
		EndIf
	WEnd
	DllClose($hDLL)
EndFunc   ;==>ButtonAddClick

Func ButtonDelClick()
	IniDelete($IniFile, GUICtrlRead($ListEntries))
	RefreshList()
EndFunc   ;==>ButtonDelClick

Func Form1Close()
	GUISetState(@SW_HIDE)
	$LoadIni = 1
EndFunc   ;==>Form1Close

Func SaveClick()
	While 1
		If StringRight(GUICtrlRead($InputTitle), 1) = " " Then
			GUICtrlSetData($InputTitle, StringTrimRight(GUICtrlRead($InputTitle), 1))
		ElseIf StringLeft(GUICtrlRead($InputTitle), 1) = " " Then
			GUICtrlSetData($InputTitle, StringTrimLeft(GUICtrlRead($InputTitle), 1))
		Else
			ExitLoop
		EndIf
	WEnd

	If GUICtrlRead($InputTitle) <> "" Then
		IniDelete($IniFile, GUICtrlRead($ListEntries))
		IniWriteSection($IniFile, GUICtrlRead($InputTitle), "")
		If GUICtrlRead($InputClass) <> "" Then IniWrite($IniFile, GUICtrlRead($InputTitle), "Class", GUICtrlRead($InputClass))
		$Match = _ArraySearch($MatchModes, GUICtrlRead($ComboMatch))
		If $Match <> 3 Then IniWrite($IniFile, GUICtrlRead($InputTitle), "Match", $Match)
		If GUICtrlRead($InputResX) > 0 Then IniWrite($IniFile, GUICtrlRead($InputTitle), "ResX", GUICtrlRead($InputResX))
		If GUICtrlRead($InputResY) > 0 Then IniWrite($IniFile, GUICtrlRead($InputTitle), "ResY", GUICtrlRead($InputResY))
		If GUICtrlRead($InputPosX) <> 0 Then IniWrite($IniFile, GUICtrlRead($InputTitle), "PosX", GUICtrlRead($InputPosX))
		If GUICtrlRead($InputPosY) <> 0 Then IniWrite($IniFile, GUICtrlRead($InputTitle), "PosY", GUICtrlRead($InputPosY))
		If GUICtrlRead($InputDelay) > 0 Then IniWrite($IniFile, GUICtrlRead($InputTitle), "Wait", GUICtrlRead($InputDelay))
		If GUICtrlRead($InputBorderFix) > 0 Then IniWrite($IniFile, GUICtrlRead($InputTitle), "Border", GUICtrlRead($InputBorderFix))

		$Mode = 0
		If BitAND(GUICtrlRead($CheckResize), 1) = 1 Then $Mode += 1
		If BitAND(GUICtrlRead($CheckActive), 1) = 1 Then $Mode += 2
		If BitAND(GUICtrlRead($CheckMouseHide), 1) = 1 Then $Mode += 4
		If BitAND(GUICtrlRead($CheckMove), 1) = 1 Then $Mode += 8
		If BitAND(GUICtrlRead($CheckFocus), 1) = 1 Then $Mode += 16
		If BitAND(GUICtrlRead($CheckMouseTrap), 1) = 1 Then $Mode += 32
		If BitAND(GUICtrlRead($CheckExstyle), 1) = 1 Then $Mode += 64
		If BitAND(GUICtrlRead($CheckNotitle), 1) = 1 Then $Mode += 128
		If $Mode > 0 Then IniWrite($IniFile, GUICtrlRead($InputTitle), "Mode", $Mode)

		RefreshList()
		GUICtrlSetData($ListEntries, GUICtrlRead($InputTitle))
		RefreshGui()
	Else
		ToolTip("Fenster Titel darf nicht leer sein!", MouseGetPos(0), MouseGetPos(1), "Fenestrierer", 1, 0)
		Sleep(3000)
		ToolTip("")
	EndIf

EndFunc   ;==>SaveClick

Func RefreshList()
	GUICtrlSetData($ListEntries, "|" & _ArrayToString(IniReadSectionNames($IniFile), "|", 1))
EndFunc   ;==>RefreshList

Func RefreshGui()
	GUICtrlSetData($InputTitle, GUICtrlRead($ListEntries))
	GUICtrlSetData($ComboMatch, $MatchModes[IniRead($IniFile, GUICtrlRead($ListEntries), "Match", 3)])
	GUICtrlSetData($InputClass, IniRead($IniFile, GUICtrlRead($ListEntries), "Class", ""))
	GUICtrlSetData($InputResX, IniRead($IniFile, GUICtrlRead($ListEntries), "ResX", "0"))
	GUICtrlSetData($InputResY, IniRead($IniFile, GUICtrlRead($ListEntries), "ResY", "0"))
	GUICtrlSetData($InputPosX, IniRead($IniFile, GUICtrlRead($ListEntries), "PosX", "0"))
	GUICtrlSetData($InputPosY, IniRead($IniFile, GUICtrlRead($ListEntries), "PosY", "0"))
	GUICtrlSetData($InputDelay, IniRead($IniFile, GUICtrlRead($ListEntries), "Wait", "0"))
	GUICtrlSetData($InputBorderFix, IniRead($IniFile, GUICtrlRead($ListEntries), "Border", "0"))

	GUICtrlSetState($CheckResize, 4)
	GUICtrlSetState($CheckActive, 4)
	GUICtrlSetState($CheckMouseHide, 4)
	GUICtrlSetState($CheckMove, 4)
	GUICtrlSetState($CheckFocus, 4)
	GUICtrlSetState($CheckMouseTrap, 4)
	GUICtrlSetState($CheckExstyle, 4)
	GUICtrlSetState($CheckNotitle, 4)

	$Mode = IniRead($IniFile, GUICtrlRead($ListEntries), "Mode", "0")
	If BitAND($Mode, 1) > 0 Then GUICtrlSetState($CheckResize, 1)
	If BitAND($Mode, 2) > 0 Then GUICtrlSetState($CheckActive, 1)
	If BitAND($Mode, 4) > 0 Then GUICtrlSetState($CheckMouseHide, 1)
	If BitAND($Mode, 8) > 0 Then GUICtrlSetState($CheckMove, 1)
	If BitAND($Mode, 16) > 0 Then GUICtrlSetState($CheckFocus, 1)
	If BitAND($Mode, 32) > 0 Then GUICtrlSetState($CheckMouseTrap, 1)
	If BitAND($Mode, 64) > 0 Then GUICtrlSetState($CheckExstyle, 1)
	If BitAND($Mode, 128) > 0 Then GUICtrlSetState($CheckNotitle, 1)
EndFunc   ;==>RefreshGui

Func CompareTitle($String, $SubString, $MatchMode = 3, $CaseSense = 2)
	Switch $MatchMode
		Case 0 ;Anfang
			If StringInStr($String, $SubString, $CaseSense, 1, 1, StringLen($SubString)) Then Return 1
		Case 1 ;Ende
			If StringInStr($String, $SubString, $CaseSense, 1, StringLen($String) - StringLen($SubString)) Then Return 1
		Case 2 ;Mitte
			If StringInStr($String, $SubString, $CaseSense) Then Return 1
		Case 3 ;Genau
			If StringCompare($String, $SubString, $CaseSense) = 0 Then Return 1
		Case Else
			Return 0
	EndSwitch
EndFunc   ;==>CompareTitle

Func CreateIni()
	Local $ini = FileOpen($IniFile, 2)
	FileWrite($ini, "[Platzhalter]" & @CRLF & @CRLF)
	FileClose($ini)
	LoadIni()
EndFunc   ;==>CreateIni

Func LoadIni()
	Local $NewName = IniReadSectionNames($IniFile)
	If @error Then
		CreateIni()
	Else
		ReDim $ProgName[UBound($NewName)]
		ReDim $ProgStat[UBound($NewName)]
		ReDim $ProgPosX[UBound($NewName)]
		ReDim $ProgPosY[UBound($NewName)]
		ReDim $ProgResX[UBound($NewName)]
		ReDim $ProgResY[UBound($NewName)]
		ReDim $ProgBfix[UBound($NewName)]
		ReDim $ProgWait[UBound($NewName)]
		ReDim $ProgMode[UBound($NewName)]
		ReDim $ProgMatch[UBound($NewName)]
		ReDim $ProgClass[UBound($NewName)]

		$ProgName = $NewName
		For $i = 0 To UBound($ProgName) - 1 Step +1
			$ProgStat[$i] = 0
			$ProgPosX[$i] = IniRead($IniFile, $ProgName[$i], "PosX", 0)
			$ProgPosY[$i] = IniRead($IniFile, $ProgName[$i], "PosY", 0)
			$ProgResX[$i] = IniRead($IniFile, $ProgName[$i], "ResX", @DesktopWidth)
			$ProgResY[$i] = IniRead($IniFile, $ProgName[$i], "ResY", @DesktopHeight)
			$ProgBfix[$i] = IniRead($IniFile, $ProgName[$i], "Border", 0)
			$ProgWait[$i] = IniRead($IniFile, $ProgName[$i], "Wait", 0)
			$ProgMode[$i] = IniRead($IniFile, $ProgName[$i], "Mode", 0)
			$ProgMatch[$i] = IniRead($IniFile, $ProgName[$i], "Match", 3)
			$ProgClass[$i] = IniRead($IniFile, $ProgName[$i], "Class", "")
		Next
	EndIf
EndFunc   ;==>LoadIni

Func KillOld()
	Local $processes = ProcessList(@ScriptName)
	For $i = 1 To $processes[0][0] Step +1
		If $processes[$i][1] <> @AutoItPID Then
			ProcessClose($processes[$i][1])
		EndIf
	Next
EndFunc   ;==>KillOld
