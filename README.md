# Fenestrierer #

* Dieses Projekt ist Lizensiert unter der Creative Commons 4.0 [CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode) ([Übersicht](https://creativecommons.org/licenses/by-nc-sa/4.0/))
* Das Icon kommt vom [Tango Desktop Project](http://tango.freedesktop.org/) und ist frei verfügbar!

## Beschreibung ##

* Ein kleines in AutoIt geschriebenes Programm, welches im Hintergrund läuft und automatisch konfigurierte Fenster, insbesondere Spiele, in den rahmenlosen Fenstermodus versetzt.
* Dazu müssen die Spiele lediglich als Fenster gestartet und in die Konfiguration aufgenommen werden, was bequem in dem Konfigurationsdialog erledigt wird.
* Eine fertig kompilierte Fenestrierer.exe ist unter Downloads verfügbar.


### Optionen bei Rechtsklick auf das Tray-Symbol: ###

* Autostart - Startet das Programm zukünftig automatisch.
* Neu laden - Setzt das Programm zurück und lädt die Konfiguration neu.
* Konfigurieren - Öffnet den Einstellungsdialog.
* Über - Zeigt Informationen über das Programm.
* Beenden - Beendet das Programm.


### Optionen im Einstellungsdialog: ###

* Hinzufügen - Nimmt das anschließend angeklickte Fenster in die Konfiguration auf.
* Entfernen - Entfernt das aktuell in der Liste gewählte Programm.
* Fenster Titel - Titel des zu Fenestrierenden Fensters. (Wird ignoriert wenn 'No title' aktiv ist.).
* Fenster Klasse - Klasse des zu Fenestrierenden Fensters. (Wird ignoriert wenn leer).
* Übereinstimmung - Möglichkeit Titel und Klasse nur partitiell zu vergleichen.
* Auflösung - Gewünschte Auflösung des Fensters nach dem Fenestrieren. Im falle von 0 wird die Auflösung des Hauptbildschirms genommen.
* Position - Gewünschte Position des Fensters. 0:0 ist die obere linke Ecke des Hauptbildschirms.
* Delay - Verzögerung in Millisekunden vor dem Fenestrieren. (Um gegebenenfalls Fehler bei bestimmten Anwendungen zu umgehen).
* Rand fix - Macht das Fenster um angegebene Anzahl Pixel größer als den Bildschirm um einen möglichen Rand auszublenden.


### Kompalibitätsmodi um Probleme mit bestimmten Anwendungen zu umgehen: ###

* Force resize - Verändert die Fenstergröße in zwei Stufen. Nützlich wenn das Fenster nach dem Fenestrieren trotz richtiger Auflösung unscharf wirkt.
* Force move - Verschiebt das Fenster in zwei Stufen. Nützlich wenn nach dem Fenestrieren die Mauseingabe im Fenster falsch positioniert ist.
* Keep exstyle - Lässt ein teil der Fenstereigenschaften unverändert. Zu Testzwecken. Vielleicht Nutzlos.
* Set active - Setzt das Fenster als aktiv. Zu Testzwecken. Vielleicht Nutzlos.
* Set focus - Holt das Fenster in den Fokus. Nützlich falls eine Anwendung beim Fenestrieren den Fokus verliert.
* No title - Nur Fensterklasse vergleichen. Nützlich wenn das Fenster keinen Titel hat oder dieser sich verändert.
* Hide mouse - Während die Anwendung im Fokus ist wird der Mauszeiger unsichtbar.
* Trap mouse - Während die Anwendung im Fokus ist wird die Maus in der Anwendung gehalten.